import os
import json

try:
    import winreg  # type: ignore # pylint: disable=import-error

except:
    import _winreg as winreg  # type: ignore # pylint: disable=import-error


def get_dropbox_location(path_type="root_path"):
    """
    Get Local path of dropbox business install.


    "root_path" = Dropbox Root path
    "path" = Dropbox User path

    :param str path_type: "root_path" or "path"
    """

    roots_path = [os.getenv("APPDATA"), os.getenv("LOCALAPPDATA")]

    for root_path in roots_path:
        dbx_local_path = os.path.join(root_path, r"Dropbox\info.json")
        if os.path.exists(dbx_local_path):
            with open(dbx_local_path, "r") as f:
                dbx_data_info = json.load(f)

            if dbx_data_info.get("business"):
                return dbx_data_info["business"][path_type]


def get_maya_location(version):
    """
    Get Maya Location from registry

    :param int version: maya version
    :return: Root Path of Maya
    """

    try:
        maya_key = r"SOFTWARE\Autodesk\Maya\{0}\Setup\InstallPath".format(version)
        maya_acces_key = winreg.OpenKey(winreg.HKEY_LOCAL_MACHINE, maya_key)
        maya_location = winreg.QueryValueEx(maya_acces_key, "MAYA_INSTALL_LOCATION")

        return maya_location[0]

    except:
        print("Maya{} not found".format(version))
        return None


def get_zbrush_location(version):
    """
    Get Zbrush Location from registry

    :param int version: zbrush version
    :return: Root Path of Maya
    """

    try:
        zbrush_key = r"SOFTWARE\Pixologic\Zbrush {}".format(version)
        zbrush_acces_key = winreg.OpenKey(winreg.HKEY_LOCAL_MACHINE, zbrush_key)
        zbrush_location = winreg.QueryValueEx(zbrush_acces_key, "LOCATION")

        return zbrush_location[0]

    except:
        print("Zbrush {} not found".format(version))
        return None
